package main

func main() {
	a := App{}
	a.Initialize()
	PopulateDBToManualTesting()
	a.Run(":443", "server.crt", "server.key")
}
