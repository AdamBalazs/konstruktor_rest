package main

import (
	"database/sql"
	"encoding/json"
	"github.com/astaxie/beego/orm"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// Application context
type App struct {
	Router *mux.Router
	DB     *sql.DB
	Config Config
	Orm    *orm.Ormer
}

// Initialize app to test or to run.
func (a *App) Initialize() {

	rand.Seed(time.Now().UTC().UnixNano())

	log.SetLevel(log.DebugLevel)
	formatter := LogFormat{}
	formatter.TimestampFormat = "2006-01-02 15:04:05"
	log.SetFormatter(&formatter)
	logFile, err := os.OpenFile("logs/logrus.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		mw := io.MultiWriter(os.Stdout, logFile)
		log.SetOutput(mw)
	} else {
		log.Info("Failed to log to file, using default stderr")
	}

	a.Router = mux.NewRouter()
	a.initializeRoutes()

	orm.Debug = false
	orm.RegisterModel(new(User))
	orm.RegisterModel(new(BlogEntry))
	orm.RegisterModel(new(Comment))
	err = orm.RegisterDataBase("default", "sqlite3", "mydb.db")
	if err != nil {
		log.Fatalf("Fatal database connection error: %s", err)
	}
	err = orm.SetDataBaseTZ("default", time.Now().Location())
	if err != nil {
		log.Fatalf("Cannot set database time to localtime: %s", err)
	}
	o := orm.NewOrm()
	a.Orm = &o

	a.Config = Config{
		JWT: ConfigJwt{
			Secret:           "your-256-bit-secret",
			ExpiresInMinutes: 60,
		},
	}
}

// Run application server.
func (a *App) Run(addr string, crt string, key string) {
	log.Fatal(http.ListenAndServeTLS(addr, crt, key, a.Router))
}

// Set up http routers.
func (a *App) initializeRoutes() {

	api := a.Router.PathPrefix("/api").Subrouter()
	api.HandleFunc("/activities/", a.activityController).Methods("GET")

	user := api.PathPrefix("/users").Subrouter()
	//user.HandleFunc("/{id:[0-9]+}", a.getUser).Methods("GET")
	user.HandleFunc("/login", a.loginController).Methods("POST")

	blogs := api.PathPrefix("/blogs").Subrouter()
	blogs.HandleFunc("/{blogid:[0-9]+}/comments/new", a.blogCommentController).Methods("POST")

	a.Router.NotFoundHandler = http.HandlerFunc(a.notFoundController)
}

// Controller: 404.
func (a *App) notFoundController(w http.ResponseWriter, r *http.Request) {
	respondWithError(w, http.StatusNotFound, ErrServerNotImplemented)
}

// Controller: comment.
func (a *App) blogCommentController(w http.ResponseWriter, r *http.Request) {

	// Check if request has a body.
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Debugf("ioutil.ReadAll() error: %v", err)
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		return
	}
	if len(string(body)) == 0 {
		log.Debug("post comment attempt without body")
		respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
		return
	}

	vars := mux.Vars(r)

	// Check if rest api call body payload is acceptable.
	payload := struct {
		Comment struct {
			Content string
		}
	}{}
	err = json.Unmarshal(body, &payload)

	if err != nil {
		log.Debugf("json.Decode() error: %v", err)
		respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
		return
	}

	// Check if email or password missing from the request json.
	if payload.Comment.Content == "" {
		log.Debugf("missing or incomplete request body: %#v", payload)
		respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
		return
	}

	// Authenticate user.
	tokenGot := r.Header.Get("Authorization")

	claims, ok := a.AuthenticateWithJWTToken(tokenGot, w)
	if ok != true {
		return
	}

	// Get blog entry.
	blogId, err := strconv.Atoi(vars["blogid"])
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		log.Errorf("Strconv error during raw sql result parsing %v. %v", blogId, err)
		return
	}

	blogEntry := &BlogEntry{}
	err = a.GetBlogEntry(blogId, blogEntry)

	// Return 404 if no blog entry available at all.
	if err == orm.ErrNoRows {
		respondWithError(w, http.StatusNotFound, ErrModelBlogEntry404)
		return

		// Return any other orm error.
	} else if err != nil {
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		log.Errorf("orm error during user data retrieval: %v", err)
		return
	}

	user := &User{}
	err = a.GetUserUsingEmail(claims.Username, user)

	// Return 404 if the authenticated user not exists in the database.
	if err == orm.ErrNoRows {
		respondWithError(w, http.StatusNotFound, ErrModelUser404)
		return

		// Return any other orm error.
	} else if err != nil {
		log.Errorf("orm error during user data retrieval: %v", err)
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		return
	}

	// Authorize user.

	// Check if the owner of the blog is the authenticated user.
	if user.Id != blogEntry.User.Id {
		respondWithError(w, http.StatusForbidden, ErrServerAccessDenied)
		return
	}

	// Create the comment and respond.
	_, comment, err := a.CreateComment(*user, *blogEntry, payload.Comment.Content)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		return
	}

	respondWithJSON(w, http.StatusCreated, &comment)

}

// Controller: activity.
func (a *App) activityController(w http.ResponseWriter, r *http.Request) {

	// The only way to use this controller is to filter the creation time of the activities in days.
	// Filter days is the number of the days in the past we filter by.

	// Check if FilterDays query string field exists.
	filterDaysArray, ok := r.URL.Query()["filterDays"]

	if ok && len(filterDaysArray[0]) > 0 {
		fds := filterDaysArray[0]
		filterDays, err := strconv.Atoi(fds)
		if err != nil || filterDays < 1 {
			respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
			return

			// Maximum filterDays is 25 year.
		} else if filterDays > 9125 {
			respondWithError(w, http.StatusBadRequest, ErrModelActivityFilterDaysToHigh)
			return
		}

		// Collect user activities and respond.
		foundUserActivities, err := a.CollectUserActivities(filterDays, w)

		if err != nil {
			log.Errorf("error during collecting user activities: %v", err)
			respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
			return
		}

		respondWithJSON(w, http.StatusOK, &struct {
			Result []UserActivities
		}{
			foundUserActivities,
		})

		// No valid filters present.
	} else {
		respondWithError(w, http.StatusNotFound, ErrServerNotImplemented)
	}
}

// Controller: login
func (a *App) loginController(w http.ResponseWriter, r *http.Request) {

	// Check if request has a body.
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Debugf("ioutil.ReadAll() error: %v", err)
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		return
	}
	if len(string(body)) == 0 {
		log.Debug("post login attempt without body")
		respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
		return
	}

	// Try to unmarshal request body into a predefined login json.
	var payload = &LoginUsingJWTSigningLoginPayload{}
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Debugf("failed to unmarshal the request body, error: %v", err)
		respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
		return
	}

	// Check if email or password missing from the request json.
	if payload.Email == "" || payload.Password == "" {
		payloadMaskedPassword := maskPassword(payload)
		log.Debugf("missing or incomplete request body: %#v", payloadMaskedPassword)
		respondWithError(w, http.StatusBadRequest, ErrServerBadRequest)
		return
	}

	// Challenge password with email.
	_, err = a.GetUserIdUsingEmailAndPassword(payload.Email, payload.Password)
	if err != nil {
		payloadMaskedPassword := maskPassword(payload)
		log.Debugf("GetUserIdUsingEmailAndPassword(%#v) error: %v", payloadMaskedPassword, err)
		respondWithError(w, http.StatusBadRequest, err)
		return
	}

	// Create JWT token.
	expires := time.Now().Add(time.Minute * time.Duration(a.Config.JWT.ExpiresInMinutes))
	token, err := CreateJWTToken(payload.Email, expires.Unix(), a.Config.JWT.Secret)
	if err != nil {
		log.Debugf("failed to sign JWT token, error: %v", err)
		respondWithError(w, http.StatusInternalServerError, ErrServerInternalError)
		return
	}
	// Respond with JWT token.
	respondWithJSON(w, http.StatusOK, LoginResponseJWT{
		Email:   payload.Email,
		Token:   token,
		Expires: expires,
	})
}

// Authenticate user using JWT token.
func (a *App) AuthenticateWithJWTToken(auth string, w http.ResponseWriter) (*JWTClaims, bool) {

	// auth should be a bearer header as a string.

	// Check if auth empty
	if auth == "" {
		respondWithError(w, http.StatusForbidden, ErrServerMissingToken)
		return nil, false
	}

	// Get the token from auth.
	split := strings.Split(auth, " ")
	if len(split) != 2 {
		respondWithError(w, http.StatusForbidden, ErrServerMalformedToken)
		return nil, false
	}
	auth = split[1]

	claims := &JWTClaims{}

	// Validate token.
	token, err := jwt.ParseWithClaims(auth, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(a.Config.JWT.Secret), nil
	})

	if err != nil {
		log.Errorf("jwt token validation error: %v", err)
		respondWithError(w, http.StatusForbidden, err)
		return nil, false
	}

	if !token.Valid {
		respondWithError(w, http.StatusForbidden, ErrServerInvalidToken)
		return nil, false
	}

	// Return the JWT payload
	return claims, true
}
