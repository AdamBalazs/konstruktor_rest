# konstruktor (online) Go próbafeladat - v1.1

## Build the container:
```bash
docker build -t konstruktor-docker .
```

## Running the server
```
mkdir -p ~/logs/konstruktor-docker
docker run -d -p 443:443 -v ~/logs/konstruktor-docker:/root/logs konstruktor-docker
```

## Manual testing

### Login user
```
## Request
curl -k --location --request POST 'https://localhost/api/users/login' \
--header 'Content-Type: application/json' \
--data-raw '{"email": "aaa@localhost", "password": "PAAA"}'

# Response
{"email":"aaa@localhost","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFhYUBsb2NhbGhvc3QiLCJleHAiOjE1ODA4MjY0MDF9.b3XnWRdhAZYeeJxvTfqNyA1EgkviS0ec8XGa_eCXGXI","expires":"2020-02-04T15:26:41.2436859+01:00"}
```

### Comment
```
# Request
curl -k --location --request POST 'https://localhost/api/blogs/1/comments/new' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFhYUBsb2NhbGhvc3QiLCJleHAiOjE1ODA4MjY0MDF9.b3XnWRdhAZYeeJxvTfqNyA1EgkviS0ec8XGa_eCXGXI' \
--header 'Content-Type: application/json' \
--data-raw '{
	"Comment": {
		"Content": "Sample text <b>unescaped</b>"
	}
}'

# Response
{
    "Id": 11,
    "User": {
        "Id": 1,
        "name": "AAA",
        "Email": "aaa@localhost"
    },
    "BlogEntry": {
        "Id": 1,
        "Title": "UserA's blog #1",
        "Created": "2020-02-04T01:26:15.4590249Z"
    },
    "content": "Sample text <b>unescaped</b>",
    "Created": "2020-02-04T14:29:42.9504125+01:00"
}
```

### Get activities
```
# Request
curl -k --location --request GET 'https://localhost/api/activities/?filterDays=1'

# Response
{
    "Result": [
        {
            "User": {
                "Id": 1,
                "name": "AAA",
                "Email": "aaa@localhost"
            },
            "Activities": {
                "BlogEntries": 1,
                "Comments": 4
            }
        },
        {
            "User": {
                "Id": 2,
                "name": "BBB",
                "Email": "bbb@localhost"
            },
            "Activities": {
                "BlogEntries": 1,
                "Comments": 4
            }
        }
    ]
}
```

## Entities
- User
  - id
  - name
  - password
  
- Blog
  - id
  - userId
  - title

- Comment
  - id
  - userId
  - blogId
  - comment
 

#### sqlite3 on Windows 10
https://github.com/mattn/go-sqlite3/issues/435

FWIW, I played with getting a build to work in the Win10 Unix tools but gave up after time wasted. Instead, I recorded these steps in my project README. They've worked several times.

Download and install tdm64-gcc-5.1.0-2.exe from http://tdm-gcc.tdragon.net/download.
Go to Program Files and click on "MinGW Command Prompt". This will open a console with the correct environment for using MinGW with GCC.
Within this console, navigate to your GOPATH.
Enter the following commands:
```
go get -u github.com/mattn/go-sqlite3
go install github.com/mattn/go-sqlite3
```
  
## Sources:
https://github.com/astaxie/beego/tree/master
https://golang.org/pkg/database/sql/
https://github.com/astaxie/beego/tree/master/orm
