package main

// TODO: Config from yml / env

type ConfigJwt struct {
	Secret           string
	ExpiresInMinutes int
}
type Config struct {
	JWT ConfigJwt
}
