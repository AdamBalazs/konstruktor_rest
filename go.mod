module konstruktor_rest

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/arschles/assert v1.0.0 // indirect
	github.com/arschles/testsrv v0.0.0-20180912204121-dd99f7516eca
	github.com/astaxie/beego v1.12.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	google.golang.org/appengine v1.6.5 // indirect
)
