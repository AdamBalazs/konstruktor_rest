package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

// Logrus custom formatter
type LogFormat struct {
	TimestampFormat string
}

func (f *LogFormat) Format(entry *log.Entry) ([]byte, error) {
	var b *bytes.Buffer

	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	b.WriteByte('[')
	b.WriteString(strings.ToUpper(entry.Level.String()))
	b.WriteString("]:")
	b.WriteString(entry.Time.Format(f.TimestampFormat))

	if entry.Message != "" {
		b.WriteString(" - ")
		b.WriteString(entry.Message)
	}

	if len(entry.Data) > 0 {
		b.WriteString(" || ")
	}
	for key, value := range entry.Data {
		b.WriteString(key)
		b.WriteByte('=')
		b.WriteByte('{')
		_, err := fmt.Fprint(b, value)
		if err != nil {
			fmt.Printf("Logrus formatter error: %v\n", err)
		}
		b.WriteString("}, ")
	}

	b.WriteByte('\n')
	return b.Bytes(), nil
}

// Helper function for masking passwords in the logs.
func maskPassword(payload *LoginUsingJWTSigningLoginPayload) *LoginUsingJWTSigningLoginPayload {
	payloadMaskedPassword := payload
	if payloadMaskedPassword.Password != "" {
		payloadMaskedPassword.Password = strings.Repeat("*", len(payload.Password))
	}
	return payloadMaskedPassword
}

// Helper function for create error response as a json object and http response code.
func respondWithError(w http.ResponseWriter, code int, message error) {
	w.Header().Set("Content-Type", "application/json")
	respondWithJSON(w, code, ErrorToJSON(message))
}

// Helper function for marshal an error to a json error object.
func ErrorToJSON(message error) map[string]string {
	return map[string]string{"error": fmt.Sprintf("%v", message)}
}

// Helper function for create non-erroneous response as a json object and http response code.
func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {

	response, err := json.Marshal(payload)
	if err != nil {
		log.Fatalf("Cannot marshal `%v` err=%v", payload, err)
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(code)

	_, err = w.Write(response)
	if err != nil {
		log.Errorf("Write error while responding with json: err=%v", err)
	}
}

// Helper function for generating a hash of a string representing a password.
func HashPassword(p string) (string, error) {
	textPassword, err := Password(p).Hash()
	return textPassword, err
}
