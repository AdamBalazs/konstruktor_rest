package main_test

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego/orm"
	"io"
	. "konstruktor_rest"
	"net/http"
	"net/http/httptest"
	"os"
	"regexp"
	"strconv"
	"testing"
	"time"
)

// Scenarios used in commentAttempt tests.
const (
	// comment scenario: comment without token.
	csNoToken = iota

	// comment scenario: comment with bad token.
	csBadToken

	// comment scenario: comment with nominal token.
	csNominal
)

var a App

func TestMain(m *testing.M) {
	a = App{}
	a.Initialize()
	os.Exit(m.Run())
}

func TestAbsoluteMeaninglessApiCallTriggers404(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerNotImplemented)
	checkApiCall(t, "GET", "https://localhost/not-gonna-happen", nil, EmptyBody{}, http.StatusNotFound, expectedErr)
}

func TestMissingActivityFiltersTriggers404(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerNotImplemented)
	checkApiCall(t, "GET", "https://localhost/api/activities/", nil, EmptyBody{}, http.StatusNotFound, expectedErr)
}

func TestEmptyActivityFilterFilterDaysTriggers404(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerNotImplemented)
	checkApiCall(t, "GET", "https://localhost/api/activities/?filterDays=", nil, EmptyBody{}, http.StatusNotFound, expectedErr)
}

func TestMalformedActivityFilterFilterDaysTriggers400(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerBadRequest)
	checkApiCall(t, "GET", "https://localhost/api/activities/?filterDays=x", nil, EmptyBody{}, http.StatusBadRequest, expectedErr)
	checkApiCall(t, "GET", "https://localhost/api/activities/?filterDays=0", nil, EmptyBody{}, http.StatusBadRequest, expectedErr)
	checkApiCall(t, "GET", "https://localhost/api/activities/?filterDays=-1", nil, EmptyBody{}, http.StatusBadRequest, expectedErr)
}

func TestNormalActivityFilterFilterDaysResponseEmpty(t *testing.T) {
	testWithMockedDb(t, func(t *testing.T, mock sqlmock.Sqlmock) {

		for i := 0; i < 2; i++ {
			mock.ExpectQuery("SELECT user_id as id, count\\(user_id\\) as c_id FROM .* WHERE `created` >= .* GROUP BY user_id").
				WillReturnRows(
					sqlmock.NewRows([]string{"id", "c_id"}))
		}

		checkApiCall(t, "GET", "https://localhost/api/activities/?filterDays=1", nil, EmptyBody{}, http.StatusOK, `{"Result":\[\]}`)

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestNormalActivityFilterFilterDaysResponse(t *testing.T) {
	testWithMockedDb(t, func(t *testing.T, mock sqlmock.Sqlmock) {

		for i := 0; i < 2; i++ {
			mock.ExpectQuery("SELECT user_id as id, count\\(user_id\\) as c_id FROM .* WHERE `created` >= .* GROUP BY user_id").
				WillReturnRows(
					sqlmock.NewRows([]string{"id", "c_id"}).
						AddRow(1, 1))
		}
		mock.ExpectQuery("SELECT `id`, `name`, `password`, `email` FROM `user` WHERE `id` = .*").
			WillReturnRows(
				sqlmock.NewRows([]string{`id`, `name`, `password`, `email`}).
					AddRow(1, "some name", "some password", "some email"))

		checkApiCall(t, "GET", "https://localhost/api/activities/?filterDays=1", nil, EmptyBody{}, http.StatusOK, `{"Result":\[{"User":{"Id":1,"name":"some name","Email":"some email"},"Activities":{"BlogEntries":1,"Comments":1}}\]}`)

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestLoginAttemptWithoutABodyTriggers400(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerBadRequest)
	checkApiCall(t, "POST", "https://localhost/api/users/login", nil, EmptyBody{}, http.StatusBadRequest, expectedErr)
}

func TestLoginAttemptWithtMalformedBodyTriggers400(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerBadRequest)
	body := marshalBody(t, map[string]interface{}{"not-gonna": "happen"})
	checkApiCall(t, "POST", "https://localhost/api/users/login", nil, bytes.NewBuffer(body), http.StatusBadRequest, expectedErr)
	body = marshalBody(t, map[string]interface{}{"email": "some@email"})
	checkApiCall(t, "POST", "https://localhost/api/users/login", nil, bytes.NewBuffer(body), http.StatusBadRequest, expectedErr)
	body = marshalBody(t, map[string]interface{}{"password": "some password"})
	checkApiCall(t, "POST", "https://localhost/api/users/login", nil, bytes.NewBuffer(body), http.StatusBadRequest, expectedErr)
}

func TestNormalLoginAttemptResponse(t *testing.T) {

	testWithMockedDb(t, func(t *testing.T, mock sqlmock.Sqlmock) {

		email := "some email"
		password := Password("some password")
		passwordHash, err := password.Hash()
		if err != nil {
			t.Errorf("password.Hash() error: %s", err)
		}
		expectedReturnBody := `^{"email":"` + email + `","token":"[^"]+\.[^"]+\.[^"]+","expires":"` + AnyTime{}.RexexStr() + `"}$`

		mock.ExpectQuery("SELECT T0.`id`, T0.`name`, T0.`password`, T0.`email` FROM `user` T0 WHERE T0.`email` = .* LIMIT 1").
			WillReturnRows(
				sqlmock.NewRows([]string{"id", "name", "password", "email"}).
					AddRow(0, "", passwordHash, email))

		body := marshalBody(t, map[string]interface{}{"email": email, "password": password})
		checkApiCall(t, "POST", "https://localhost/api/users/login", nil, bytes.NewBuffer(body), http.StatusOK, expectedReturnBody)

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func TestCommentAttemptWithoutABodyTriggers400(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerBadRequest)
	checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", nil, EmptyBody{}, http.StatusBadRequest, expectedErr)
}

func TestCommentAttemptWithtMalformedBodyTriggers400(t *testing.T) {
	expectedErr := getExpectedErr(t, ErrServerBadRequest)
	body := marshalBody(t, map[string]interface{}{"not-gonna": "happen"})
	checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", nil, bytes.NewBuffer(body), http.StatusBadRequest, expectedErr)
	body = marshalBody(t, map[string]interface{}{"comment": nil})
	checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", nil, bytes.NewBuffer(body), http.StatusBadRequest, expectedErr)
	body = marshalBody(t, map[string]interface{}{"comment": map[string]interface{}{"content": ""}})
	checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", nil, bytes.NewBuffer(body), http.StatusBadRequest, expectedErr)
}

func TestNormalCommentAttemptResponse(t *testing.T) {
	commentAttempt(csNominal, t)
}

func TestCommentAttemptResponseWithoutToken(t *testing.T) {
	commentAttempt(csNoToken, t)
}

func TestCommentAttemptResponseWithBadToken(t *testing.T) {
	commentAttempt(csBadToken, t)
}

// Test helper function for create different comment attempts.
func commentAttempt(scenario int, t *testing.T) {
	if scenario != csNominal && scenario != csBadToken && scenario != csNoToken {
		t.Errorf("unknown scenario in comment attempt: %v", scenario)
	} else {
		testWithMockedDb(t, func(t *testing.T, mock sqlmock.Sqlmock) {
			// List of query parameters
			var qp []driver.Value

			// Result of the mock queries
			mr := struct {
				title        string
				email        string
				passwordHash string
				name         string
				content      string
				userId       int64
				blogEntryId  int64
				lastInsertID int64
			}{
				title:        "some title",
				email:        "some email",
				passwordHash: "some password hash",
				name:         "some name",
				content:      "some content",
				userId:       1,
				blogEntryId:  1,
				lastInsertID: 1,
			}

			var expectedResultBody string

			if scenario == csNominal {
				expectedResultBody = `{"Id":` + strconv.Itoa(int(mr.lastInsertID)) + `,"User":{"Id":` + strconv.Itoa(int(mr.userId)) + `,"name":"` + mr.name + `","Email":"` + mr.email + `"},"BlogEntry":{"Id":` + strconv.Itoa(int(mr.blogEntryId)) + `,"Title":"` + mr.title + `","Created":"` + AnyTime{}.RexexStr() + `"},"content":"` + mr.content + `","Created":"` + AnyTime{}.RexexStr() + `"}`

				// Expected Blog entry select
				qp = []driver.Value{mr.userId}
				mock.ExpectQuery("SELECT .* FROM `blog_entry` T0 WHERE T0.`id` = ?").
					WithArgs(qp[0]).
					WillReturnRows(
						sqlmock.NewRows([]string{"id", "user_id", "title", "created"}).
							AddRow(mr.blogEntryId, mr.userId, mr.title, time.Now()))
				qp = []driver.Value{mr.email}

				// Expected user select
				mock.ExpectQuery("SELECT .* FROM `user` T0 WHERE T0.`email` = ?").
					WithArgs(qp[0]).
					WillReturnRows(
						sqlmock.NewRows([]string{"id", "name", "password", "email"}).
							AddRow(mr.userId, mr.name, mr.passwordHash, mr.email))

				// Expected comment insert
				mock.ExpectExec("INSERT INTO `comment`").WithArgs(mr.userId, mr.blogEntryId, mr.content, AnyTime{}).
					WillReturnResult(sqlmock.NewResult(mr.lastInsertID, 1))
			}

			//TODO: mock jwt signing

			// Create JWT
			headers := createToken(scenario, mr.email, t)

			body := marshalBody(t, map[string]interface{}{"comment": map[string]interface{}{"content": mr.content}})

			// Testing nominal comment scenario.
			if scenario == csNominal {
				checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", headers, bytes.NewBuffer(body), http.StatusCreated, expectedResultBody)

				// Testing comment attempt without Authorization header.
			} else if scenario == csNoToken {
				expectedErr := getExpectedErr(t, ErrServerMissingToken)
				checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", headers, bytes.NewBuffer(body), http.StatusForbidden, expectedErr)

				// Testing comment attempt without Proper Authorization header.
			} else if scenario == csBadToken {
				checkApiCall(t, "POST", "https://localhost/api/blogs/1/comments/new", headers, bytes.NewBuffer(body), http.StatusForbidden, "")
			}

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}

// Test helper function for stub ORM db connection.
func testWithMockedDb(t *testing.T, f func(t *testing.T, mock sqlmock.Sqlmock)) {

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Errorf("failed to stub database connection, error: %v", err)
	}

	err = orm.SetDataBaseTZ("default", time.Now().Location())
	if err != nil {
		t.Errorf("failed to stub database connection, error: %v", err)
	}

	o, err := orm.NewOrmWithDB("sqlite3", "default", db)
	if err != nil {
		t.Errorf("failed to stub database connection, error: %v", err)
	}

	// inject mock orm to App container
	a.Orm = &o

	defer db.Close()

	// Run the mockable function
	f(t, mock)
}

// Test helper function for record a request for testing.
func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)
	return rr
}

// EmptyBody represents an empty io.ReadCloser used in the tests as a empty request body.
type EmptyBody struct{}

func (e EmptyBody) Read(p []byte) (n int, err error) {
	return 0, io.EOF
}
func (e EmptyBody) Close() error {
	return nil
}

// AnyTime represents any legal time.Time representable value.
type AnyTime struct{}

// Match satisfies go-sqlmock Argument interface so usable in the as a query parameter to match with any time.
func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

// Return an "any-time" code regex pattern to use in a json response match.
func (a AnyTime) RexexStr() string {
	return `[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[^"]+`
}

// Test helper function return an err as a json error object string representation.
func getExpectedErr(t *testing.T, err error) string {
	expectedErr, err := json.Marshal(ErrorToJSON(err))
	if err != nil {
		t.Errorf("failed to marshal map, error: %v", err)
	}
	return string(expectedErr)
}

// Test helper function for marshal a map.
func marshalBody(t *testing.T, m map[string]interface{}) []byte {
	body, err := json.Marshal(m)
	if err != nil {
		t.Errorf("failed to marshal map, error: %v", err)
	}
	return body
}

// Test helper function for create different token based scenarios in a request.
func createToken(scenario int, email string, t *testing.T) map[string]string {
	expires := time.Now().Add(time.Minute * time.Duration(a.Config.JWT.ExpiresInMinutes))
	token, err := CreateJWTToken(email, expires.Unix(), a.Config.JWT.Secret)
	if err != nil {
		t.Errorf("failed to sign JWT token, error: %v", err)
	}

	if scenario == csNominal {
		return map[string]string{"Authorization": "Bearer " + token}
	} else if scenario == csBadToken {
		return map[string]string{"Authorization": "Bearer noope"}
	} else {
		return map[string]string{}
	}
}

// Test helper function for check http return code and result body of rest api calls.
func checkApiCall(t *testing.T, method string, url string, headers map[string]string, body io.Reader, expectedReturnCode int, expectedReturnBody string) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		t.Errorf("http.NewRequest returned error:%v", err)
	}

	for header, value := range headers {
		req.Header.Add(header, value)
	}

	response := executeRequest(req)
	if response.Code != expectedReturnCode {
		t.Errorf("expected response code is %d. Got %d", expectedReturnCode, response.Code)
	}

	if expectedReturnBody != "" {
		matched, err := regexp.Match(expectedReturnBody, response.Body.Bytes())
		if err != nil {
			t.Errorf("regexp.Match returned error:%v", err)
		}
		if !matched {
			t.Errorf("expected response body shopuld match witn \\%v\\, got `%v`", expectedReturnBody, string(response.Body.Bytes()))
		}
	}
}
