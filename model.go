package main

import (
	"errors"
	"fmt"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"net/http"
	"runtime"
	"strconv"
	"time"
)

// User entity.
type User struct {
	Id          int          `orm:"auto"`
	BlogEntries []*BlogEntry `orm:"reverse(many)" json:"-"`
	Comments    []*Comment   `orm:"reverse(many)" json:"-"`
	Name        string       `json:"name"`
	Password    string       `json:"-"`
	Email       string       `orm:"unique;index"`
}

// BlogEntry entity.
type BlogEntry struct {
	Id       int        `orm:"auto"`
	User     *User      `orm:"rel(fk)" json:"-"`
	Comments []*Comment `orm:"reverse(many)" json:"-"`
	Title    string
	Created  time.Time `orm:"type(datetime)"` //auto_now_add;
}

// Comment entity.
type Comment struct {
	Id        int        `orm:"auto"`
	User      *User      `orm:"rel(fk)"`
	BlogEntry *BlogEntry `orm:"rel(fk)"`
	Content   string     `json:"content"`
	Created   time.Time  `orm:"type(datetime)"` //auto_now_add;
}

// Return User entity Id using email and password to filter.
func (a *App) GetUserIdUsingEmailAndPassword(email string, password string) (int, error) {

	u := User{}

	// Get User entity using email as filter.
	err := a.GetUserUsingEmail(email, &u)

	if err == orm.ErrNoRows {
		return 0, ErrModelUser404
	} else if err != nil {
		log.Fatalf("Orm error during user data retrieval: %v", err)
	}

	// Challenge password.
	p := Password(password)
	err = p.CompareHash(u.Password)
	if err != nil {
		return 0, ErrModelUserPasswordMismatch
	} else {
		return u.Id, nil
	}
}

// Get User entity using email as filter.
func (a *App) GetUserUsingEmail(email string, u *User) error {
	err := (*a.Orm).QueryTable("user").Filter("email", email).One(u)
	return err
}

// Get BlogEntry entity using id as filter.
func (a *App) GetBlogEntry(id int, b *BlogEntry) error {
	err := (*a.Orm).QueryTable(BlogEntry{}).Filter("id", id).One(b)
	return err
}

// JSON representation of the user activity result set.
type UserActivities struct {
	User       User
	Activities Activities
}

// Part of the JSON representation of the user activity result set.
type Activities struct {
	BlogEntries int
	Comments    int
}

// Return collected user activities based on time filter.
func (a *App) CollectUserActivities(filterDays int, w http.ResponseWriter) ([]UserActivities, error) {

	// Map contains the result of the filter query for ParseRawActivitiesFilterQuery().
	var countedValues []orm.Params

	// userActivitiesMap contains the result of ParseRawActivitiesFilterQuery().
	userActivitiesMap := map[int]*Activities{}

	// Calculate user blog activities.
	q := "SELECT user_id as id, count(user_id) as c_id FROM `blog_entry` WHERE `created` >= ? GROUP BY user_id"
	num, err := (*a.Orm).Raw(q, NDaysAgo(filterDays)).Values(&countedValues)
	if err == nil && num > 0 {
		if ifErr := ParseRawActivitiesFilterQuery(userActivitiesMap, countedValues, "blogEntries"); ifErr != nil {
			return nil, ifErr
		}
	}

	// Calculate user commenting activity.
	countedValues = []orm.Params{}
	q = "SELECT user_id as id, count(user_id) as c_id FROM `comment` WHERE `created` >= ? GROUP BY user_id"
	num, err = (*a.Orm).Raw(q, NDaysAgo(filterDays)).Values(&countedValues)
	if err == nil && num > 0 {
		if ifErr := ParseRawActivitiesFilterQuery(userActivitiesMap, countedValues, "comments"); ifErr != nil {
			return nil, ifErr
		}
	}

	// Aggregate user data with the found activities.
	foundUserActivities := []UserActivities{}
	for userId, activities := range userActivitiesMap {
		user := User{Id: userId}
		err := (*a.Orm).Read(&user)
		if err == orm.ErrNoRows {
			log.Errorf("user activity without existing user found in the database. userId: %v", userId)
			continue
		} else if err != nil {
			return nil, errors.New(fmt.Sprintf("Error during User entity loading: %v", err))
		}
		foundUserActivities = append(foundUserActivities, UserActivities{
			User:       user,
			Activities: *activities,
		})
	}
	return foundUserActivities, nil
}

// Return the current local time minus n days.
func NDaysAgo(n int) time.Time {
	if n == 0 {
		return time.Now()
	}
	return time.Now().Add(-time.Duration(n) * 24 * time.Hour)
}

// Parse the user activity raw queries and convert them to Activities struct data.
func ParseRawActivitiesFilterQuery(ret map[int]*Activities, rawValues []orm.Params, activity string) error {
	for _, rows := range rawValues {

		rowsId := fmt.Sprintf("%v", rows["id"])
		rowsCid := fmt.Sprintf("%v", rows["c_id"])

		id, err := strconv.Atoi(rowsId)
		if err != nil {
			return errors.New(fmt.Sprintf("Strconv error during raw sql result parsing %v. %v", rowsId, err))
		}

		cId, err := strconv.Atoi(rowsCid)
		if err != nil {
			return errors.New(fmt.Sprintf("Strconv error during raw sql result parsing %v. %v", rowsCid, err))
		}

		if ret[id] == nil {
			ret[id] = &Activities{}
		}

		switch activity {
		case "blogEntries":
			ret[id].BlogEntries = cId
			break
		case "comments":
			ret[id].Comments = cId
			break
		default:
			return errors.New(fmt.Sprintf("unimplemented activity: %v", activity))
		}
	}
	return nil
}

func (a *App) CreateComment(user User, blogEntry BlogEntry, comment string) (int64, *Comment, error) {
	c := &Comment{
		Id:        0,
		User:      &user,
		BlogEntry: &blogEntry,
		Content:   comment,
		Created:   time.Now(),
	}
	i, err := (*a.Orm).Insert(c)
	return i, c, err
}

func PopulateDBToManualTesting() {
	o := orm.NewOrm()

	err := orm.RunSyncdb("default", true, false)
	if err != nil {
		log.Fatalf("DB Sync error: %v", err)
	}

	passwordA, err := HashPassword("PAAA")
	if err != nil {
		log.Fatalf("Password hashing error: %v", err)
	}
	userA := User{Name: "AAA", Password: passwordA, Email: "aaa@localhost"}

	passwordB, err := HashPassword("PBBB")
	if err != nil {
		log.Fatalf("Password hashing error: %v", err)
	}
	userB := User{Name: "BBB", Password: passwordB, Email: "bbb@localhost"}

	passwordC, err := HashPassword("PCCC")
	if err != nil {
		log.Fatalf("Password hashing error: %v", err)
	}
	userC := User{Name: "CCC", Password: passwordC, Email: "ccc@localhost"}

	users := []*User{
		&userA,
		&userB,
		&userC,
	}

	// I really hope there is a better way to do this...
	for _, u := range users {
		i, err := o.Insert(u)
		if err != nil {
			_, fn, line, _ := runtime.Caller(1)
			panic(fmt.Sprintf("[error] %s:%d %v", fn, line, err))
		}
		u.Id = int(i)
	}

	blogEntryA := BlogEntry{User: &userA, Title: "UserA's blog #1", Created: time.Now().Add(-12 * time.Hour)}
	blogEntryA2 := BlogEntry{User: &userA, Title: "UserA's blog #2", Created: time.Now().Add(-24 * time.Hour)}
	blogEntryB := BlogEntry{User: &userB, Title: "UserB's blog #1", Created: time.Now().Add(-24 * 2 * time.Hour)}

	blogEntries := []*BlogEntry{
		&blogEntryA,
		&blogEntryA2,
		&blogEntryB,
	}
	// I really hope there is a better way to do this...
	for _, u := range blogEntries {
		i, err := o.Insert(u)
		if err != nil {
			_, fn, line, _ := runtime.Caller(1)
			panic(fmt.Sprintf("[error] %s:%d %v", fn, line, err))
		}
		u.Id = int(i)
	}

	commentAA1 := Comment{User: &userA, BlogEntry: &blogEntryA, Content: "Content A A 1", Created: time.Now().Add(-12 * time.Hour)}
	commentAA2 := Comment{User: &userA, BlogEntry: &blogEntryA, Content: "Content A A 2", Created: time.Now().Add(-24 * time.Hour)}
	commentAA3 := Comment{User: &userA, BlogEntry: &blogEntryA, Content: "Content A A 3", Created: time.Now().Add(-24 * 2 * time.Hour)}
	commentAB1 := Comment{User: &userA, BlogEntry: &blogEntryB, Content: "Content A B 1", Created: time.Now().Add(-24 * 3 * time.Hour)}
	commentAB2 := Comment{User: &userA, BlogEntry: &blogEntryB, Content: "Content A B 2", Created: time.Now().Add(-24 * 4 * time.Hour)}
	commentA2A1 := Comment{User: &userA, BlogEntry: &blogEntryA2, Content: "Content A A2 1", Created: time.Now().Add(-12 * time.Hour)}
	commentBA1 := Comment{User: &userB, BlogEntry: &blogEntryA, Content: "Content B A 1", Created: time.Now().Add(-24 * time.Hour)}
	commentBA2 := Comment{User: &userB, BlogEntry: &blogEntryA, Content: "Content B A 2", Created: time.Now().Add(-24 * 2 * time.Hour)}
	commentBB1 := Comment{User: &userB, BlogEntry: &blogEntryB, Content: "Content B B 1", Created: time.Now().Add(-24 * 3 * time.Hour)}
	commentBB2 := Comment{User: &userB, BlogEntry: &blogEntryB, Content: "Content B B 2", Created: time.Now().Add(-24 * 4 * time.Hour)}
	comments := []*Comment{
		&commentAA1,
		&commentAA2,
		&commentAA3,
		&commentAB1,
		&commentAB2,
		&commentBA1,
		&commentBA2,
		&commentBB1,
		&commentBB2,
		&commentA2A1,
	}
	// I really hope there is a better way to do this...
	for _, u := range comments {
		i, err := o.Insert(u)
		if err != nil {
			_, fn, line, _ := runtime.Caller(1)
			panic(fmt.Sprintf("[error] %s:%d %v", fn, line, err))
		}
		u.Id = int(i)
	}
}
