package main

import (
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"time"
)

// Post body of user login api call.
type LoginUsingJWTSigningLoginPayload struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Response body of user login api call.
type LoginResponseJWT struct {
	Email   string    `json:"email"`
	Token   string    `json:"token"`
	Expires time.Time `json:"expires"`
}

// JWT token claims.
type JWTClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

// Password used in login.
type Password string

// Generate password hash.
func (p Password) Hash() (string, error) {
	r, err := bcrypt.GenerateFromPassword([]byte(p), 10)
	return string(r), err
}

// Compare password hash.
func (p Password) CompareHash(h string) error {
	return bcrypt.CompareHashAndPassword([]byte(h), []byte(p))
}

func CreateJWTToken(email string, expiredAt int64, secret string) (string, error) {

	claims := &JWTClaims{
		Username: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredAt,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(secret))
}
