FROM alpine:edge as builder
LABEL maintainer="adam@localhost"
RUN apk update
RUN apk upgrade
RUN apk add --update go gcc g++
WORKDIR /app
COPY . .
RUN go mod download
RUN go test . -v
RUN CGO_ENABLED=1 GOOS=linux go install -a .

FROM alpine:edge
ARG LOG_DIR=/root/logs
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /root/go/bin/konstruktor_rest .
RUN apk add --update openssl
RUN openssl genrsa -out server.key 2048
RUN openssl ecparam -genkey -name secp384r1 -out server.key
RUN openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650 -batch
RUN mkdir -p ${LOG_DIR}
RUN rm -f ${LOG_DIR}/*
VOLUME ["${LOG_DIR}"]
EXPOSE 443
CMD ["/root/konstruktor_rest"]