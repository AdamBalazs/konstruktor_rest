package main

import "errors"

var (
	ErrModelActivityFilterDaysToHigh = errors.New("filterdays is to high")
	ErrModelBlogEntry404             = errors.New("blog not found")
	ErrModelUser404                  = errors.New("user not found")
	ErrModelUserPasswordMismatch     = errors.New("user not found")
	ErrServerAccessDenied            = errors.New("access denied for commenting")
	ErrServerBadRequest              = errors.New("invalid request")
	ErrServerInternalError           = errors.New("internal server error")
	ErrServerInvalidToken            = errors.New("invalid authentication token")
	ErrServerInvalidUserId           = errors.New("invalid user id")
	ErrServerMalformedToken          = errors.New("malformed authentication token")
	ErrServerMissingToken            = errors.New("missing authentication token")
	ErrServerNotImplemented          = errors.New("not implemented")
)
